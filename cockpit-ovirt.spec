# Used for rpm-packaging of pre-bundled application with already resolved JS dependencies
%global _plugindir %{_datarootdir}/cockpit/
%global _ovirtenginedir %{_datarootdir}/ovirt-engine
%global product oVirt
%global use_rhev %(test -z oVirt && echo 1 || echo 0)
%global yarn_install no
%define debug_package %{nil}


%global source_basename cockpit-ovirt-0.15.0

Name:           cockpit-ovirt
Version:        0.15.0
Release:        4
Summary:        Dashboard for Cockpit based on %{product}
License:        ASL 2.0
URL:            https://gerrit.ovirt.org/gitweb?p=cockpit-ovirt.git;a=summary
Source0:        http://resources.ovirt.org/pub/src/%{name}/%{source_basename}.tar.gz



# nodejs-modules embeds yarn and requires nodejs
BuildRequires: ovirt-engine-nodejs-modules >= 2.0.45

%if 0%{?enable_autotools}
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	gettext-devel
%endif

%package dashboard
Summary:        Dashboard for Cockpit based on %{product}
BuildArch:      noarch

Requires:       cockpit
Requires:       cockpit-storaged
Requires:       ovirt-hosted-engine-setup >= 2.5.0
Requires:       otopi >= 1.9.0

Requires:       ansible

%description
This package provides a Cockpit dashboard for use with %{product}.

%description dashboard
This package provides a Cockpit dashboard for use with %{product}.

%prep
%setup -q -n %{source_basename}

%if "%{yarn_install}" == "yes"
%{warn:Building the rpm with a yarn online install is dangerous, use of ovirt-engine-nodejs-modules is highly recommended.}
%else
# Setup yarn and the nodejs-modules via ovirt-engine-nodejs-modules
pushd dashboard
source %{_datadir}/ovirt-engine-nodejs-modules/setup-env.sh
popd
%endif

%build
%if 0%{?enable_autotools}
autoreconf -ivf
%endif

export PATH="%{_datadir}/ovirt-engine-nodejs-modules/bin:${PATH}"

%if "%{yarn_install}" == "yes"
%configure --with-yarn-install
%else
%configure
%endif

# Change hardcoded deafult hash algorithm from legacy md4 to sha512
sed -i 's/md4/sha512/g' dashboard/node_modules/webpack/lib/*.js
sed -i 's/md4/sha512/g' dashboard/node_modules/webpack/lib/optimize/*.js
sed -i 's/md4/sha512/g' dashboard/node_modules/terser-webpack-plugin/dist/index.js
sed -i 's/md4/sha512/g' dashboard/node_modules/copy-webpack-plugin/dist/postProcessPattern.js
sed -i 's/md4/sha512/g' dashboard/node_modules/babel-loader/lib/cache.js

make

%install
make install DESTDIR=%{buildroot}

# Create /var/lib/ovirt-hosted-engine-setup/cockpit
install -dm 700 %{buildroot}%{_sharedstatedir}/ovirt-hosted-engine-setup/cockpit

%files dashboard
%doc README.md
%license LICENSE
%{_plugindir}/ovirt-dashboard
%dir %attr(700, root, root) %{_sharedstatedir}/ovirt-hosted-engine-setup/cockpit

%changelog
* Mon Apr 01 2024 yanjianqing <yanjianqing@kylinos.cn> - 0.15.0-4
- Remove cockpit conf file

* Thu Mar 02 2023 wangdi <wangdi@kylinos.cn> - 0.15.0-3
- Change hardcoded deafult hash algorithm from legacy md4 to sha512
- Change dependecy from ansible-2.9 to ansible since ansible version has bumped to 2.9

* Fri Sep 24 2021 wutao <wutao61@huawei.com> - 0.15.0-2
- change ansible to ansible-2.9

* Tue Jul 06 2021 ningjin <ningjin@kylinos.cn> - 0.15.0-1
- package init
